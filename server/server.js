const app = require('express')();
const http = require('http').createServer(app);
// const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const dayjs = require('dayjs')
var relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(relativeTime)
// const path = require('path');
require('dotenv').config();
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, PATCH, DELETE, OPTIONS')
    next()
})

const io = require("socket.io")(http, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

let numUsers = 0;


io.on('connection', (socket) => {

    console.log('new connection made.');


    socket.on('join', function (data) {
        //joining
        socket.join(data.room);

        console.log(data.user + 'joined the room : ' + data.room);

        socket.broadcast.to(data.room).emit('new user joined', { user: data.user, message: 'has joined this room.' });
    });


    socket.on('leave', function (data) {

        console.log(data.user + 'left the room : ' + data.room);

        socket.broadcast.to(data.room).emit('left room', { user: data.user, message: 'has left this room.' });

        socket.leave(data.room);
    });

    socket.on('message', function (data) {
        console.log(data)
        io.in(data.room).emit('new message', { user: data.user, message: data.message, sent_at: dayjs().format('MMMM D, YYYY h:mm A') });

    })

});


http.listen(port, () => {
    console.log(`listening on *:${port}`);
});