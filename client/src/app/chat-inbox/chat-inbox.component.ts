import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat-inbox',
  templateUrl: './chat-inbox.component.html',
  styleUrls: ['./chat-inbox.component.css'],
  providers: [ChatService]
})
export class ChatInboxComponent implements OnInit {

  public show: boolean = false;
  public buttonName: any = 'Show';
  user: String;
  room: String;
  messageText: String;
  messageArray: Array<{ user: String, message: String }> = [];
  constructor(private _chatService: ChatService) {
    this._chatService.newUserJoined()
      .subscribe(data => this.messageArray.push(data));


    this._chatService.userLeftRoom()
      .subscribe(data => this.messageArray.push(data));

    this._chatService.newMessageReceived()
      .subscribe(data => this.messageArray.push(data));
  }

  join() {
    if (!this.user && !this.room) {

    } else {
      this._chatService.joinRoom({ user: this.user, room: this.room });
      this.show = !this.show;
    }

  }

  leave() {
    this._chatService.leaveRoom({ user: this.user, room: this.room });
    this.messageArray = [];
    this.show = !this.show;
  }

  sendMessage() {
    this._chatService.sendMessage({ user: this.user, room: this.room, message: this.messageText });
    this.messageText = ""
  }

  ngOnInit(): void {
  }

}
